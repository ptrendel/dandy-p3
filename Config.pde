static class Config {

  static final int MODE_EDIT = 0;
  static final int MODE_TEXTURE = 1;
  static final int MODE_DISPLAY = 2;

  static final int TEXTURE_MODE_BG_COLOR = 0;
  static final int TEXTURE_MODE_STROKE_COLOR = 1;
  static final int TEXTURE_MODE_STROKE_WEIGHT = 2;
  static final int TEXTURE_MODE_SPRITE = 3;
  static final int TEXTURE_MODE_ANIMATION = 4;
  static final int TEXTURE_MODE_CAMERA = 5;



  static final int ARTEFACT_DEFAULT_SIZE = 100;
  static final int ARTEFACT_DEFAULT_POSITION_X = 100;
  static final int ARTEFACT_DEFAULT_POSITION_Y = 100;
  static final int ARTEFACT_DEFAULT_STROKE_COLOR = #FFFFFF;
  static final int ARTEFACT_DEFAULT_STROKE_WEIGHT = 1;
  static final int ARTEFACT_STROKE_LIMIT = 10;
  static final int ARTEFACT_DEFAULT_COLOR = #0000FF;

  static final int HANDLE_SIZE = 10;
  static final color HANDLE_COLOR = 0xCCFFFF00;
  static final color HANDLE_SELECTED_COLOR = 0xCCFF0000;

  static final int MODE_GAMEPAD = 0;
  static final int MODE_KEYBOARD = 1;
  static final int DEFAULT_INPUT_MODE = MODE_GAMEPAD; 

  Config() {
  }
}