class ColorTexture implements Texture {

  color texture;
  String TYPE;

  ColorTexture(PApplet sketch) {
    TYPE = "COLOR";
  }

  void setSource(color c) {
    texture = c;
  }

  void setSource(PImage img) {
  }
  void setSource(PImage[] img) {
  }

  String getType() {
    return TYPE;
  }


  color renderColor() {
    return texture;
  }
  PImage render() {
    return null;
  }
}