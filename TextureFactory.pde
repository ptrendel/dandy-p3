public class TextureFactory {
  
    PApplet sketch;
  
   TextureFactory(PApplet sketch){
     this.sketch = sketch;
   }
  
   //use getShape method to get object of type shape 
   public Texture getTexture(String textureType){
      if(textureType == null){
         return null;
      } 
      if(textureType.equalsIgnoreCase("COLOR")){
         return new ColorTexture(this.sketch);
         
      }else if(textureType.equalsIgnoreCase("IMAGE")){
         return new ImageTexture(this.sketch);
         
      } else if(textureType.equalsIgnoreCase("ANIMATION")){
         return new AnimationTexture(this.sketch);
         
      } else if(textureType.equalsIgnoreCase("CAMERA")){
         return new CameraTexture(this.sketch);
      }
      
      return null;
   }
}