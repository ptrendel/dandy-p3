class CameraTexture implements Texture {

  CameraSource source;
  Capture cam;
  String TYPE;

  CameraTexture(PApplet sketch) { 
    source = CameraSource.getInstance(sketch);
    cam = source.getCameraInstance();
    TYPE = "CAMERA";
  }

  void setSource(PImage img) {
  } 

  void setSource(color c) {
  }
  String getType() {
    return TYPE;
  }
  void setSource(PImage[] img) {
  }

  color getColor() {
    return -1;
  }

  color renderColor(){return  -1;}
  
  PImage render() {

    if (cam.available() == true) {
      cam.read();
    }

    return cam;
  }
}