class UIManager {
  PApplet sketch;
  PFont f;
  PVector position;
  PVector section;
  int margin;
  int w;
  int innerwidth;
  int h;
  int row;
  RessourceLoader r;
  int textureIndex = 0;
  boolean lock=false;
  Texture[] textures;

  int BUTTON_HEIGHT = 40;
  color BUTTON_BACKGROUND = color(255, 100);
  color BUTTON_TEXT_COLOR = color(0);

  public UIManager(PApplet s) {
    f = createFont("Lucida Console", 30, false); 
    w = 200;
    h = height;
    margin = 20;
    innerwidth = w-margin*2;
    position = new PVector(width - w, 0);
    row = 0;
    sketch = s;
    r = new RessourceLoader(sketch);
    textures = r.getTextures();
    textureIndex = 0;
  }

  void listen() {
    if (!keyPressed) return;
    if ( key != CODED ) {
      switch(key) {
      case 'v': 
        textureIndex ++;
        keyPressed = false;
        break;
      case 'c': 
        textureIndex --;
        keyPressed = false;  
        break;
      /*case DOWN:
        textureIndex +=4;
        keyPressed = false;  
        break;
      case UP:
        textureIndex -=4;
        keyPressed = false;  
        break;*/
      }
    }
  }


  void button(String txt, int row) {
    PVector centerPoint = new PVector(position.x+margin+(innerwidth/2), position.y+margin+row*(BUTTON_HEIGHT+margin)+BUTTON_HEIGHT/2);

    fill(BUTTON_BACKGROUND);
    rect (position.x+margin, position.y+margin+row*(BUTTON_HEIGHT+margin), innerwidth, BUTTON_HEIGHT);

    fill(BUTTON_TEXT_COLOR);
    textAlign(CENTER, CENTER);
    text(txt, centerPoint.x, centerPoint.y);
  }

  Texture getTexture() {    
    return textures[textureIndex];
  }

  void textureIncrement(int inc){
    int value = this.textureIndex + inc;
    if(value < 0) {
      this.textureIndex = 0;
    }else if( value > textures.length-1) {
      this.textureIndex = textures.length-1;
    }else{
      this.textureIndex = value;
    }
  }


  void display() {
    //listen();

    pushMatrix();
    translate(position.x+20, position.y+20);

    Texture[] textures = r.getTextures();
    int j=0;
    for (int i=0; i<textures.length; i++) {
    
      // Highlight selected texture  
      if (i==textureIndex) { strokeWeight(2);stroke(0); } else { strokeWeight(0);noStroke(); }
    
      // Make a 4 column grid
      if (i!=0 && i%4 == 0) j+=40;
      
      // Display thumbnail for each texture type
      beginShape();
      textureMode(NORMAL);
      if (textures[i].getType()=="COLOR") {
        fill(textures[i].renderColor());
        //rect((i%4)*40, j, 35, 35);
      } else if (textures[i].getType()=="IMAGE" || textures[i].getType()=="ANIMATION" || textures[i].getType()=="CAMERA") {
        PImage frame = textures[i].render(); 
        texture(frame);
      }
      vertex((i%4)*40 , j , 0 , 0);
      vertex((i%4)*40 , j + 35 , 1 , 0);
      vertex((i%4)*40 + 35 , j + 35 , 1 , 1);
      vertex((i%4)*40 +35 , j , 0 , 1);
      
      endShape(CLOSE);
    }

    popMatrix();
  }


}