public static class CameraSource {

  Capture cam;
  PApplet sketch;

  private static CameraSource instance;

  public static CameraSource getInstance(PApplet sketch) {
    if (instance != null)
      return instance;

    instance = new CameraSource(sketch);
    return instance;
  }

  private CameraSource(PApplet sketch) {
    this.sketch = sketch;
    String[] cameras = Capture.list();
      println("Create Camera");
      if (cameras.length > 0) {
        this.cam = new Capture(this.sketch, cameras[1]);
        this.cam.start();
      }
  }

  public Capture getCameraInstance() {
    return cam;
  }
}