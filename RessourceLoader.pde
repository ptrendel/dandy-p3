class RessourceLoader {
  PApplet sketch;
  color[] colors;
  PImage[] sprites;
  PImage[][] animations;
  
  Texture[] textures;

  public RessourceLoader(PApplet s) {

    sketch = s;
    JSONArray ressourceData = loadJSONArray("ressource.json");

    //Load Color Palette
    JSONObject colorObj = ressourceData.getJSONObject(0);
    JSONArray colorPalette = colorObj.getJSONArray("colors");
    int colorCount = colorPalette.size();

    // Load Sprites
    PImage spriteSheet = loadImage("spritesheet.png");

    JSONObject spritesObject = ressourceData.getJSONObject(1);
    JSONArray spritesArray = spritesObject.getJSONArray("sprites");
    int spriteCount = spritesArray.size();

    //Load Animation

    JSONObject animationsObject = ressourceData.getJSONObject(2);
    JSONArray animationsArray = animationsObject.getJSONArray("animations");
    int animationCount = animationsArray.size();
    
    int textureCount = 0;
    textures = new Texture[colorCount + spriteCount + animationCount + 1];
    TextureFactory tfactory = new TextureFactory(sketch);
    
    
    //
    
    colors = new color[colorCount];
    for (int i=0; i<colorCount; i++) {
      JSONObject colorObject = colorPalette.getJSONObject(i);
      color c = color(colorObject.getInt("r"), colorObject.getInt("v"), colorObject.getInt("b"));
      colors[i] = c;
      Texture t = tfactory.getTexture("COLOR");
      t.setSource(c);
      textures[textureCount] = t;
      textureCount++;
      
    }

    sprites = new PImage[spriteCount];
    for (int i=0; i< spriteCount; i++) {
      JSONObject f = spritesArray.getJSONObject(i);
      int posX = f.getJSONObject("frame").getInt("x");
      int posY = f.getJSONObject("frame").getInt("y");
      int spriteWidth = f.getJSONObject("sourceSize").getInt("w");
      int spriteHeight = f.getJSONObject("sourceSize").getInt("h"); 
      PImage sp  = spriteSheet.get(posX, posY, spriteWidth, spriteHeight);
      sprites[i] = sp;
      Texture t = tfactory.getTexture("IMAGE");
      t.setSource(sp);
      textures[textureCount] = t;
      textureCount++;
    }

    animations = new PImage[animationCount][];
    
    for (int i=0; i<animationCount; i++) {
      JSONObject anim = animationsArray.getJSONObject(i);
      String fname = anim.getString("filename");
      int row = anim.getJSONObject("frame").getInt("row");
      int col = anim.getJSONObject("frame").getInt("col");
      int w = anim.getJSONObject("frame").getInt("h");
      int h = anim.getJSONObject("frame").getInt("h");
      animations[i] = new PImage[row*col];
      Texture t = tfactory.getTexture("ANIMATION");
      PImage src = loadImage(fname);
      for (int k=0; k<row; k++) {
        for (int j=0; j<col; j++) {
          int index= j+k*col;
          PImage frame = src.get(k*w, j*h, w, h);
          animations[i][index] = frame;
          
        }
      }
      t.setSource (animations[i]);
      textures[textureCount] = t;
      textureCount ++;
    }
    
    textures[textureCount] = tfactory.getTexture("CAMERA");
    
    
  }

  public color[] getColors() {
    return this.colors;
  }

  public PImage[] getSprites() {
    return this.sprites;
  }

  public PImage[][] getAnimations() {
    return this.animations;
  }
  
  public Texture[] getTextures() {
    return this.textures;
  }
}