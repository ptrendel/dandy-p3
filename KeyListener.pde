class KeyListener {

  Scene scene;

  KeyListener(PApplet p, Scene s) {
    this.scene = s;
  }

  void record(boolean kpressed) {
    if (!kpressed) return;

    int deltaX = 0;
    int deltaY = 0;
    int increment = 2;

    if (key !=CODED && key=='b') {
      println(this.scene.MODE);
      if (this.scene.MODE == Config.MODE_EDIT ) {
        this.scene.MODE = Config.MODE_TEXTURE;
      } else if (this.scene.MODE == Config.MODE_TEXTURE ) {
        this.scene.MODE = Config.MODE_EDIT;
      }
      keyPressed = false;
    }

    if (this.scene.MODE == Config.MODE_EDIT) {
      if ( key == CODED ) {
        switch(keyCode) {
        case RIGHT: 
          deltaX = increment; 
          deltaY = 0;
          break;
        case LEFT: 
          deltaX = -increment; 
          deltaY = 0;  
          break;
        case UP: 
          deltaX = 0; 
          deltaY = -increment;  
          break;
        case DOWN: 
          deltaX = 0; 
          deltaY = increment;  
          break;
        }
        if (this.scene.selectedIndex >-1) {
          Artefact a = this.scene.elements.get(this.scene.selectedIndex);


          if (a.selectedHandleIndex > -1) {
            Handle h = a.handles[a.selectedHandleIndex];
            a.moveHandle(a.selectedHandleIndex, int(h.position.x) + deltaX, int(h.position.y) + deltaY);
          } else {
            a.move(deltaX, deltaY);
          }
        }
      } else {
        if (key=='a') {
          this.scene.addElement();  
          keyPressed = false;
        }
        if (this.scene.selectedIndex >-1) {
          Artefact a = this.scene.elements.get(this.scene.selectedIndex);
          switch(key) {
          case 'd': 
            this.scene.getNextArtefact();  
            keyPressed = false;
            break;
          case 'q': 
            this.scene.getPreviousArtefact();  
            keyPressed = false;
            break;
          case 'z': 
            a.selectNextHandle(); 
            keyPressed = false;
            break;
          case 's': 
            a.selectPreviousHandle(); 
            keyPressed = false;
            break;
            /*case 'p': 
             a.texture.upStroke(); 
             keyPressed = false;
             break;
             case 'm': 
             a.texture.downStroke(); 
             keyPressed = false;
             break;   
             case 'o': 
             a.texture.TEXTURE_MODE = Config.TEXTURE_MODE_BG_COLOR;
             a.texture.upColor(); 
             keyPressed = false;
             break;
             case 'l': 
             a.texture.TEXTURE_MODE = Config.TEXTURE_MODE_BG_COLOR;
             a.texture.downColor(); 
             keyPressed = false;
             break;   
             case 'i': 
             a.texture.TEXTURE_MODE = Config.TEXTURE_MODE_STROKE_COLOR;
             a.texture.upColor(); 
             keyPressed = false;
             break;
             case 'k': 
             a.texture.TEXTURE_MODE = Config.TEXTURE_MODE_STROKE_COLOR;
             a.texture.downColor(); 
             keyPressed = false;
             break;     
             case 'u': 
             a.texture.TEXTURE_MODE = Config.TEXTURE_MODE_SPRITE;
             a.texture.upSprite(); 
             keyPressed = false;
             break;
             case 'j': 
             a.texture.TEXTURE_MODE = Config.TEXTURE_MODE_SPRITE;
             a.texture.downSprite(); 
             keyPressed = false;
             break;        
             case 'y': 
             a.texture.TEXTURE_MODE = Config.TEXTURE_MODE_ANIMATION;
             a.texture.upAnimation(); 
             keyPressed = false;
             break;
             case 'h': 
             a.texture.TEXTURE_MODE = Config.TEXTURE_MODE_ANIMATION;
             a.texture.downAnimation(); 
             keyPressed = false;
             break;        */
          }
        }
      }
    }
  }
}