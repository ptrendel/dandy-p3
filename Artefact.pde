class Artefact {
  PApplet app;
  Scene scene;
  int id;
  PShape shape;
  Handle[] handles;
  int selectedHandleIndex;
  int subdiv;

  Texture texture;

  Artefact (PApplet p, Scene s, int aid) {
    println("Artefact : New");
    this.app = p;
    this.scene = s;
    this.id = aid;
    this.handles = new Handle[4];
    this.selectedHandleIndex = -1;
    TextureFactory tfactory = new TextureFactory(this.app);
    this.texture = tfactory.getTexture("COLOR");
    this.texture.setSource(color(255,0,135));
    this.subdiv = 10;
    handles[0] = new Handle(this.app, this.scene, 10, 10);
    handles[1] = new Handle(this.app, this.scene, Config.ARTEFACT_DEFAULT_SIZE, 10);
    handles[2] = new Handle(this.app, this.scene, Config.ARTEFACT_DEFAULT_SIZE, Config.ARTEFACT_DEFAULT_SIZE);
    handles[3] = new Handle(this.app, this.scene, 10, Config.ARTEFACT_DEFAULT_SIZE);

    render();
  }

  void setTexture(Texture t) {
    this.texture = t;    
  }
  
  void drawShape(){
      this.shape = createShape();
      if (this.texture.getType() == "COLOR") {
        this.shape.beginShape();
        this.shape.fill(this.texture.renderColor());
        this.shape.vertex(this.handles[0].position.x, this.handles[0].position.y);
        this.shape.vertex(this.handles[1].position.x, this.handles[1].position.y);
        this.shape.vertex(this.handles[2].position.x, this.handles[2].position.y);
        this.shape.vertex(this.handles[3].position.x, this.handles[3].position.y);
        this.shape.endShape(CLOSE);
      } else {
        this.shape.beginShape();
        this.shape.texture(this.texture.render());
        this.shape.vertex(this.handles[0].position.x, this.handles[0].position.y, 0, 0);
        this.shape.vertex(this.handles[1].position.x, this.handles[1].position.y, 1, 0);
        this.shape.vertex(this.handles[2].position.x, this.handles[2].position.y, 1, 1);
        this.shape.vertex(this.handles[3].position.x, this.handles[3].position.y, 0, 1);
        this.shape.endShape(CLOSE);
      }
      shape(this.shape);
  }
  
  void warpRender() {
    PVector[] vertices = this.getVertices();
    int dim = subdiv+1;
    noFill();
    noStroke();
    
    
    if (this.texture.getType()=="COLOR") {
      beginShape();
      fill(this.texture.renderColor());
      
      vertex(this.handles[0].position.x,this.handles[0].position.y);
      vertex(this.handles[1].position.x,this.handles[1].position.y);
      vertex(this.handles[2].position.x,this.handles[2].position.y);
      vertex(this.handles[3].position.x,this.handles[3].position.y);
      endShape(CLOSE);
    } else {

      textureMode(NORMAL);
      beginShape(QUAD);
      texture(this.texture.render());
      float norm = 1.0/subdiv;

      for (int r=0; r<dim-1; r++) {

        for (int c=0; c<dim-1; c++) {

          int idx = (r*dim)+c;
          vertex(vertices[idx].x, vertices[idx].y, norm*c, norm*r);
          vertex(vertices[idx+1].x, vertices[idx+1].y, norm*(c+1), norm*r);
          vertex(vertices[idx+1+dim].x, vertices[idx+1+dim].y, norm*(c+1), norm*(r+1));
          vertex(vertices[idx+dim].x, vertices[idx+dim].y, norm*c, norm*(r+1));
        }
      }

      endShape();
      
    }
    
    
  }

  void render() {
    
    this.shape = createShape();
    this.shape.beginShape();   
    
    if (this.texture != null) {
      if(this.texture.getType() == "COLOR") {
        fill(this.texture.renderColor());
        this.shape.vertex( this.handles[0].position.x, this.handles[0].position.y);
        this.shape.vertex( this.handles[1].position.x, this.handles[1].position.y);
        this.shape.vertex( this.handles[2].position.x, this.handles[2].position.y);
        this.shape.vertex( this.handles[3].position.x, this.handles[3].position.y);
      }else{
        textureMode(NORMAL);
        PImage text = this.texture.render();
        texture(text);
        noFill();
        this.shape.vertex( this.handles[0].position.x, this.handles[0].position.y,0,0);
        this.shape.vertex( this.handles[1].position.x, this.handles[1].position.y,1,0);
        this.shape.vertex( this.handles[2].position.x, this.handles[2].position.y,1,1);
        this.shape.vertex( this.handles[3].position.x, this.handles[3].position.y,0,1);
      }
      
      this.shape.endShape(CLOSE);
    }
  }
  
  
  PVector[] getVertices() {
    PVector p0 = PVector.sub(this.handles[1].position, this.handles[0].position).div(subdiv);
    PVector p1 = PVector.sub(this.handles[2].position, this.handles[1].position).div(subdiv);
    PVector p2 = PVector.sub(this.handles[2].position, this.handles[3].position).div(subdiv);
    PVector p3 = PVector.sub(this.handles[3].position, this.handles[0].position).div(subdiv);

    PVector[] vertices = new PVector[int(sq(subdiv+1))];
    int index =0;
    for (int i= 0; i<subdiv+1; i++) {
      PVector v1 = PVector.mult(p3, i).add(this.handles[0].position);
      PVector v2 = PVector.mult(p1, i).add(this.handles[1].position);    
      vertices[index++] = v1;
      PVector pp = PVector.sub(v2, v1);
      for (int j= 0; j<subdiv-1; j++) {
        PVector plot = PVector.div(pp, subdiv).mult(j+1).add(v1);
        vertices[index++] = plot;
      }
      vertices[index++] = v2;
    }
    return vertices;
  }

  


  void selectHandle(int index) {
    if (this.selectedHandleIndex > -1 && this.selectedHandleIndex < this.handles.length) {
      this.handles[this.selectedHandleIndex].unselect();
    }
    this.selectedHandleIndex = index;
    if (this.selectedHandleIndex > -1 && this.selectedHandleIndex < this.handles.length) {
      this.handles[this.selectedHandleIndex].select();
    }
  }


  void selectNextHandle() {
    if (this.selectedHandleIndex > -1 && this.selectedHandleIndex < this.handles.length) {
      this.handles[this.selectedHandleIndex].unselect();
    }

    if (this.selectedHandleIndex<0) {
      this.selectedHandleIndex = 0;
    } else if (this.selectedHandleIndex < this.handles.length-1) {
      this.selectedHandleIndex ++;
    } else if (this.selectedHandleIndex == this.handles.length-1) {
      this.selectedHandleIndex = -1;
    }

    if (this.selectedHandleIndex > -1 && this.selectedHandleIndex < this.handles.length) {
      this.handles[this.selectedHandleIndex].select();
    }
  }


  void selectPreviousHandle() {
    if (this.selectedHandleIndex > -1 && this.selectedHandleIndex < this.handles.length) {
      this.handles[this.selectedHandleIndex].unselect();
    }

    if (this.selectedHandleIndex < 0) {
      this.selectedHandleIndex = this.handles.length - 1;
    } else if (this.selectedHandleIndex >= 0) {
      this.selectedHandleIndex --;
    }
    if (this.selectedHandleIndex > -1 && this.selectedHandleIndex < this.handles.length) {
      this.handles[this.selectedHandleIndex].select();
    }
  }

  void move(int deltaX, int deltaY) {
    for (int i=0; i<this.handles.length; i++) {
      moveHandle(i, int(this.handles[i].position.x)+deltaX, int(this.handles[i].position.y)+deltaY);
    }
  }

  void moveHandle(int handleIndex, int posX, int posY) {
    this.handles[handleIndex].position.x = posX;
    this.handles[handleIndex].position.y = posY;

    //this.render();
    this.drawShape();
    //this.handles[handleIndex].buildShape();
    this.handles[handleIndex].drawShape();
  }
}