class Scene {
  PApplet app;
  UIManager ui;
  ArrayList<Artefact> elements = new ArrayList<Artefact>();
  int selectedIndex;
  color backgroundColor;

  int MODE;


  Scene (PApplet p, UIManager ui) {
    this.MODE = Config.MODE_EDIT;
    this.app = p;
    this.ui = new UIManager(this.app);
    this.backgroundColor = color(255);
    selectedIndex = -1;
  }

  void addElement() {
    int nextIndex = this.elements.size();
    this.elements.add(new Artefact(app, scene, nextIndex));
    this.selectedIndex = nextIndex;
  }

  void getNextArtefact() {
    if (this.selectedIndex<this.elements.size()-1) {
      this.selectedIndex++;
    } else {
      this.selectedIndex = 0;
    }
  }

  void getPreviousArtefact() {
    if (this.selectedIndex>0) {
      this.selectedIndex--;
    } else {
      this.selectedIndex = this.elements.size()-1;
    }
  }

  void render() {
    background(this.backgroundColor);
    if(this.MODE == Config.MODE_TEXTURE && this.selectedIndex >= 0) {
      this.ui.display();
      Texture t = this.ui.getTexture();
      Artefact s = this.elements.get(this.selectedIndex);
      s.setTexture(t);
      //s.drawShape();
      //s.warpRender();
    } 
      
      for (int i=0; i<this.elements.size(); i++) {
       Artefact a = this.elements.get(i);
       //a.drawShape();
       a.warpRender();
      
       if(this.MODE != Config.MODE_DISPLAY && this.elements.get(this.selectedIndex).id == a.id) {
         for (int j=0; j < a.handles.length; j++) {
         a.handles[j].drawShape();
         }
       }
      }
    
      

    

    /*if(this.MODE == Config.MODE_TEXTURE && this.selectedIndex >= 0) {
     ui.display();
     Artefact a = this.elements.get(this.selectedIndex);
     a.setTexture(this.ui.getTexture());
     a.render();
     shape(a.shape);
     }else{
     for (int i=0; i<this.elements.size(); i++) {
     Artefact a = this.elements.get(i);
     shape(a.shape);
     
     if(this.MODE != Config.MODE_DISPLAY && this.elements.get(this.selectedIndex).id == a.id) {
     for (int j=0; j < a.handles.length; j++) {
     shape(a.handles[j].shape);
     }
     }
     
     }
     }*/
  }
}