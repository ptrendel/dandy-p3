import processing.video.*;
import net.java.games.input.*;
import org.gamecontrolplus.*;
import org.gamecontrolplus.gui.*;

Scene scene;
InputManager input;
UIManager ui;

void setup(){
  fullScreen(P2D);
  //size (854,480,P2D);
  ui = new UIManager(this);
  scene = new Scene(this,ui);
  input = new InputManager(this,scene);
}

void draw(){
  input.listen();
  scene.render();
}