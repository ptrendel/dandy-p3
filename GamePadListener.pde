class GamePadListener {
  Scene scene;
  ControlDevice device; 
  int timer;
  HashMap<String, Boolean> inputLocker = new HashMap<String, Boolean>();


  GamePadListener(PApplet p, Scene s, ControlDevice d) {
    this.scene = s;
    device = d;
    this.timer = millis();
    this.inputLocker.put("AButton", false);
    this.inputLocker.put("BButton", false);
    this.inputLocker.put("leftButton", false);
    this.inputLocker.put("rightButton", false);
    this.inputLocker.put("rightStickH", false);
    this.inputLocker.put("rightStickV", false);
    this.inputLocker.put("leftStickH", false);
    this.inputLocker.put("leftStickV", false);
    this.inputLocker.put("dPad", false);
    this.inputLocker.put("dPadUp", false);
    this.inputLocker.put("dPadDown", false);
    this.inputLocker.put("dPadLeft", false);
    this.inputLocker.put("dPadRight", false);
  }


  boolean isLocked(String locker) {
    return this.inputLocker.get(locker);
  }

  void lock(String locker) {
    this.inputLocker.put(locker, true);
  }

  void unlock(String locker) {
    this.inputLocker.put(locker, false);
  }

  void record() {
    String locker = "";

    // Add an artefact on screen With A Button

    locker = "AButton";
    if (device.getButton(locker).pressed() && !isLocked(locker) ) {
      this.lock(locker);
      this.scene.addElement();
    } else if (!device.getButton(locker).pressed() && isLocked(locker)) {
      this.unlock(locker);
    }

    // B Button : Switch between Texture and Edit mode

    locker = "BButton";
    if (device.getButton(locker).pressed() && !isLocked(locker)) {
      if (this.scene.MODE == Config.MODE_EDIT ) {
        this.scene.MODE = Config.MODE_TEXTURE;
      } else if (this.scene.MODE == Config.MODE_TEXTURE ) {
        this.scene.MODE = Config.MODE_EDIT;
      }
      this.lock(locker);
    } else if (!device.getButton(locker).pressed() && isLocked(locker)) {
      this.unlock(locker);
    }

    // Start Button : Go to display Mode

    if (device.getButton("startButton").pressed()) {
      this.scene.MODE = Config.MODE_DISPLAY;
    }

    // Back Button : Go to Edit Mode

    if (device.getButton("backButton").pressed()) {
      this.scene.MODE = Config.MODE_EDIT;
    }


    switch (this.scene.MODE) {
    case Config.MODE_EDIT :
      this.editionEventHandler();
      break;
    case Config.MODE_TEXTURE :
      this.textureEventHandler();
      break;
    case Config.MODE_DISPLAY :
      this.displayEventHandler();
      break;
    }
  }



  void editionEventHandler() {

    String locker ="";

    if (this.scene.selectedIndex >-1) {
      Artefact a = this.scene.elements.get(this.scene.selectedIndex);

      // Move Artefact with the Right Stick
      ControlSlider XSlider = device.getSlider("leftStickH");
      ControlSlider YSlider = device.getSlider("leftStickV");

      int deltaX = int(XSlider.getValue() * 10);
      int deltaY = int(YSlider.getValue() * 10);

      // Move Artefact with the DPad : Fine Tuning pixel by pixel
      locker = "dPad";
      ControlHat switcher = device.getHat(locker);
      locker = "dPadUp";
      if (switcher.up() && !isLocked(locker) ) {
        deltaY = -1;
        this.lock(locker);
      } else if (!switcher.up() && isLocked(locker)) {
        this.unlock(locker);
      }

      locker = "dPadDown";
      if (switcher.down() && !isLocked(locker) ) {
        deltaY = 1;
        this.lock(locker);
      } else if (!switcher.down() && isLocked(locker)) {
        this.unlock(locker);
      }

      locker = "dPadLeft";
      if (switcher.left() && !isLocked(locker) ) {
        deltaX = -1;
        this.lock(locker);
      } else if (!switcher.left() && isLocked(locker)) {
        this.unlock(locker);
      }

      locker = "dPadRight";
      if (switcher.right() && !isLocked(locker) ) {
        deltaX = 1;
        this.lock(locker);
      } else if (!switcher.right() && isLocked(locker)) {
        this.unlock(locker);
      }

      if (a.selectedHandleIndex > -1) {
        Handle h = a.handles[a.selectedHandleIndex];
        a.moveHandle(a.selectedHandleIndex, int(h.position.x) + deltaX, int(h.position.y) + deltaY);
      } else {
        a.move(deltaX, deltaY);
      }

      // Cycle through Artefact on screen

      locker = "leftButton";
      if (device.getButton(locker).pressed() && !isLocked(locker) ) {
        this.lock(locker);
        this.scene.getPreviousArtefact();
      } else if (!device.getButton(locker).pressed() && isLocked(locker)) {
        this.unlock(locker);
      }

      locker = "rightButton";
      if (device.getButton(locker).pressed() && !isLocked(locker) ) {
        this.lock(locker);
        this.scene.getNextArtefact();
      } else if (!device.getButton(locker).pressed() && isLocked(locker)) {
        this.unlock(locker);
      }

      // Cycle through Handle Artefact 

      ControlSlider XRightSlider = device.getSlider("rightStickH");
      ControlSlider YRightSlider = device.getSlider("rightStickV");

      boolean stickDown = YRightSlider.getValue()> 0.3;
      boolean stickUp = YRightSlider.getValue() < -0.3;
      boolean stickLeft = XRightSlider.getValue() < -0.3;
      boolean stickRight = XRightSlider.getValue() > 0.3;

      if (stickUp && stickLeft) a.selectHandle(0);
      if (stickUp && stickRight) a.selectHandle(1);     
      if (stickDown && stickRight) a.selectHandle(2);
      if (stickDown && stickLeft) a.selectHandle(3);
      if (!stickUp && !stickLeft && !stickDown && !stickRight) a.selectHandle(-1);
    }
  }

  void textureEventHandler() {


    if (this.scene.selectedIndex >-1) {
      Artefact a = this.scene.elements.get(this.scene.selectedIndex);

      ControlSlider XRightSlider = device.getSlider("rightStickH");
      ControlSlider YRightSlider = device.getSlider("rightStickV");

      boolean stickDown = YRightSlider.getValue()> 0.3;
      boolean stickUp = YRightSlider.getValue() < -0.3;
      boolean stickLeft = XRightSlider.getValue() < -0.3;
      boolean stickRight = XRightSlider.getValue() > 0.3;

      boolean unlock = false;
      int ctimer = millis();
      if (ctimer-this.timer > 100) {
        unlock = true;
      }

      if (stickUp) {
        if(unlock) {
          this.scene.ui.textureIncrement(-4);
          this.timer = ctimer;
        }
      }
      if (stickRight) {
        if(unlock) {
          this.scene.ui.textureIncrement(1);
          this.timer = ctimer;
        }
      }     
      if (stickDown) {
        if(unlock) {
          this.scene.ui.textureIncrement(4);
          this.timer = ctimer;
        }
      }
      if (stickLeft) {
        if(unlock) {
          this.scene.ui.textureIncrement(-1);
          this.timer = ctimer;
        }
      }
    }

    //this.scene.ui.textureIndex
    /*
    if (this.scene.selectedIndex >-1) {
     Artefact a = this.scene.elements.get(this.scene.selectedIndex);
     
     // Choose Texture mode
     locker = "leftStickH";
     ControlSlider LHSlider = device.getSlider(locker);
     int LHSliderValue = int(LHSlider.getValue());
     if (LHSliderValue > 0.3 && !isLocked(locker)) {
     a.texture.nextMode();
     this.lock(locker);
     } else if (LHSliderValue < -0.3 && !isLocked(locker)) {
     a.texture.previousMode();
     this.lock(locker);
     } else if (LHSliderValue > -0.3 && LHSliderValue < 0.3 && isLocked(locker)) {
     this.unlock(locker);
     }
     
     // Select texture inside mode
     locker = "rightStickV";
     ControlSlider RVSlider = device.getSlider(locker);
     int RVSliderValue = int(RVSlider.getValue());
     if (RVSliderValue > 0.3 && !isLocked(locker)) {
     
     if (a.texture.TEXTURE_MODE == Config.TEXTURE_MODE_BG_COLOR || a.texture.TEXTURE_MODE == Config.TEXTURE_MODE_STROKE_COLOR ) {
     a.texture.downColor();
     }
     if (a.texture.TEXTURE_MODE == Config.TEXTURE_MODE_STROKE_WEIGHT) {
     a.texture.downStroke();
     }
     if (a.texture.TEXTURE_MODE == Config.TEXTURE_MODE_SPRITE) {
     a.texture.downSprite();
     }
     if (a.texture.TEXTURE_MODE == Config.TEXTURE_MODE_ANIMATION) {
     a.texture.downAnimation();
     }
     
     this.lock(locker);
     } else if (RVSliderValue < -0.3 && !isLocked(locker)) {
     
     if (a.texture.TEXTURE_MODE == Config.TEXTURE_MODE_BG_COLOR || a.texture.TEXTURE_MODE == Config.TEXTURE_MODE_STROKE_COLOR ) {
     a.texture.upColor();
     }
     if (a.texture.TEXTURE_MODE == Config.TEXTURE_MODE_STROKE_WEIGHT) {
     a.texture.upStroke();
     }
     if (a.texture.TEXTURE_MODE == Config.TEXTURE_MODE_SPRITE) {
     a.texture.upSprite();
     }
     if (a.texture.TEXTURE_MODE == Config.TEXTURE_MODE_ANIMATION) {
     a.texture.upAnimation();
     }
     
     this.lock(locker);
     } else if (RVSliderValue > -0.3 && RVSliderValue < 0.3 && isLocked(locker)) {
     this.unlock(locker);
     }
     }*/
  }

  void displayEventHandler() {
  }
}