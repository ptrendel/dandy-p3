interface Texture {
  
  String TYPE = null;
  
  void setSource(PImage img);
  void setSource(PImage[] img);
  void setSource(color c);
  String getType();
  PImage render();
  color renderColor();

}