class InputManager{
  PApplet app;
  Scene scene;
  KeyListener keyListener;
  GamePadListener gamepadListener;
  ControlIO control;
  ControlDevice device;
  int INPUT_MODE;
  

  
  InputManager(PApplet p, Scene s){
    this.app = p;
    this.scene = s;
    
    this.INPUT_MODE = Config.DEFAULT_INPUT_MODE;
    
    if(this.INPUT_MODE == Config.MODE_GAMEPAD) {  
      this.control = ControlIO.getInstance(this.app);
      this.device = control.getMatchedDevice("dandy");
      this.gamepadListener = new GamePadListener(this.app,this.scene,this.device);
    }else if(this.INPUT_MODE == Config.MODE_KEYBOARD){
      this.keyListener = new KeyListener(this.app,this.scene);
    }
  }
  
  void listen(){
     if(this.INPUT_MODE == Config.MODE_GAMEPAD) {  
      gamepadListener.record();
    }else if(this.INPUT_MODE == Config.MODE_KEYBOARD){
      this.keyListener.record(keyPressed);
    }
  }
  
  
}