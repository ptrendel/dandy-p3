class Handle {
  PApplet app;
  Scene scene;
  PVector position;
  PShape shape;
  boolean isSelected;

   Handle (PApplet p, Scene s, int posX, int posY){
     println("Handle : New");
     this.app = p;
     this.scene = s;
     this.position = new PVector();
     this.setPosition(posX, posY);
     this.isSelected = false;
     this.buildShape();
   }
   
   void setPosition(int posX, int posY){
     println("Handle : Set Position "+posX+","+posY);
     this.position.x = posX;
     this.position.y = posY;
   }
   
  void drawShape(){
    buildShape();
    shape(this.shape);
  }
   
  void buildShape() {
    //println("Handle : Building Shape");
    this.shape = createShape(
      ELLIPSE, 
      this.position.x, 
      this.position.y, 
      Config.HANDLE_SIZE, 
      Config.HANDLE_SIZE
    );
    this.shape.setStroke(false);
    
    this.shape.setFill(Config.HANDLE_COLOR);
    if(this.isSelected){
       this.shape.setFill(Config.HANDLE_SELECTED_COLOR);
    }else{
       this.shape.setFill(Config.HANDLE_COLOR);
    }

  }
  
  void select(){
    this.isSelected = true;
    this.drawShape();
  }
  
  void unselect(){
    this.isSelected = false;
    this.drawShape();
  }
}