class ImageTexture implements Texture {

  PImage texture;
  String TYPE;

  ImageTexture(PApplet sketch) {
    texture = new PImage();
    TYPE = "IMAGE";
  }

  void setSource(PImage img) {
    this.texture = img;
  }

  void setSource(color c) {
  }

  void setSource(PImage[] img) {
  }

  String getType() {
    return TYPE;
  }

  color renderColor(){return  -1;}

  PImage render() {
    return this.texture;
  }
}